// Recorrer el arreglo definido en la opcion anterior y mostrarlo aplicando 4 estructuras de repeticion //
function estructuras(datos){
    console.log("Usando forEach");
    datos.forEach(comidas =>{
        console.log("nombre:%s, tipo:%s, ingredientes: %s", comidas.nombre, comidas.tipo, comidas.ingredientes);
    }
    );
}
estructuras(comidas);

let k=comidas.length;

function estructuras1(info, valor){
    console.log("Usuario for");
    for( c=0; c< valor; c++){
        console.log(info[c])
    }
}
 estructuras1(comidas,k);
 function estructuras2(info,valor){
     console.log("usuario while");
     let c=0;
     while(c< valor){
         console.log(info[c])
         c++;
        }

 }
 estructuras2(comidas,k);

 function estructuras3(info,valor){
     console.log("usuario do while");
     let c=0;
     do{
         console.log(info[c])
         c++;
     }while(c< valor);
 }
  estructuras3(comidas, k);