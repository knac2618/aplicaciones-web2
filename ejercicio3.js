// Definir una arreglo con sus comidas favoritas, teniendo como base el objeto del punto anterior /
const FAVORITAS=[
        {
            NOMBRE:'MANI CON PESCADO',
            INGREDIENTES:['PESCADO','MANI','CEBOLLA','PIMIENTO','AJO','AGUA','ACHIOTE','SAL'],
            TIPO:'PLATO FUERTE',
            VALOR:'2.75',
        },
        {
            NOMBRE:'MADURO CON QUESO',
            INGREDIENTES:['MADURO','QUESO','SAL'],
            TIPO:'ENTRADA',
            VALOR:'1.25',
        },
        {
            NOMBRE:'TORTITAS DE VERDE',
            INGREDIENTES:['PLATANO','CEBOLLA','PIMIENTO','HUEVO','AGUA','ACHIOTE','SAL','QUESO'],
            TIPO:'ENTRADA',
            VALOR:'1.50',
        },
        {
            NOMBRE:'3 LECHES ',
            INGREDIENTES:['CREMA DE LECHE','LECHE CONDENSADA','HARINA','AZUCAR'],
            TIPO:'POSTRE',
            VALOR:'4.00',
        }
    

]